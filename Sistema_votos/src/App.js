import React, { useState } from 'react';
import './App.css';
import Button from 'react-bootstrap/Button';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  const [candidates, setCandidates] = useState([
    { name: "Kauê", votes: 0 },
    { name: "Jão", votes: 0 },
    { name: "Gabriel", votes: 0 },
    { name: "Antônio", votes: 0 },
  ]);

  const [finished, setFinished] = useState(false);

  function vote(candidateIndex) {
    const newCandidates = [...candidates];
    newCandidates[candidateIndex].votes++;
    setCandidates(newCandidates);
  }

  function finishCount() {
    setFinished(true);
  }


  if (finished) {
    const winner = candidates.reduce((prev, current) =>
      (prev.votes > current.votes) ? prev : current
    );

    const sortedCandidates = [...candidates].sort((a, b) => b.votes - a.votes);

    return (
      <div className="App">
        <h1>Contagem Finalizada</h1>
        <div className="alert alert-success" role="alert">
          <h4 className="alert-heading">Vencedor: {winner.name} com {winner.votes} votos!</h4>
          <p className="mb-0"></p>
        </div>

        <table className="table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Votes</th>
            </tr>
          </thead>
          <tbody>
            {sortedCandidates.map((sortedCandidates, index) => (
              <tr key={index}>
                <th scope="row">{index + 1}</th>
                <td>{sortedCandidates.name}</td>
                <td>{sortedCandidates.votes}</td>
              </tr>
            ))}
          </tbody>

        </table>
      </div>
    );
  }

  const candidateCards = candidates.map((candidate, index) => (
    <div className="Candidate" key={index}>
      <h3>{candidate.name}</h3>
      <p>Votos: {candidate.votes}</p>
      <button type="button" className="btn btn-primary" onClick={() => vote(index)}>Votar</button>
    </div>
  ));

  return (
    <div className="App">
      <h1>Contagem de Votos</h1>
      {candidateCards}
      <br />
      <Button onClick={finishCount} variant="success">Finalizar Contagem</Button>
    </div>
  );
}

export default App;
